import {authReq} from "../configs/index.js";
import {VisitCardio, VisitDentist, VisitTherapist} from "../components/index.js"

async function getAllCards() {
    const {data} = await authReq.get('/cards');
    const cards = data.map((visit) => {
        const {type, ...content} = visit;
        if (type === 'cardio'){
            return new VisitCardio(content);
        }
        if (type === 'dentist'){
            return new VisitDentist(content);
        }
        if (type === 'therapist'){
            return new VisitTherapist(content);
        }
    });
    return cards
}

export default getAllCards