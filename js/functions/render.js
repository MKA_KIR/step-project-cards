const render = (elem, container) => container.append(elem.render());

export default render;