import Visit from "./visit.js";

class VisitDentist extends Visit {

    doctorType = "Стоматолог";

    render(){
        const patientVisit = super.render();
        this.addAdditionalInfo(patientVisit);
        this.reconciliation(patientVisit);
        return this.elem
    }

    getPatientAdditionalInfo() {
        return `<li class="patient-info-item"><u>Дата последнего посещения</u>: ${this.props.lastVisitDate || "неизвестно"}.</li>`
    }
    
}

export default VisitDentist;