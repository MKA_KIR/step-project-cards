import Visit from "./visit.js";

class VisitTherapist extends Visit {

    doctorType = "Терапевт";

    render(){
        const patientVisit = super.render();
        this.addAdditionalInfo(patientVisit);
        this.reconciliation(patientVisit);
        return this.elem
    }

    getPatientAdditionalInfo() {
        return `<li class="patient-info-item"><u>Возраст</u>: ${this.props.age}.</li>`
    }
}

export default VisitTherapist;