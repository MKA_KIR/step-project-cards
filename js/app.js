import {render, getAllCards} from "./functions/index.js";

const patientVisitsList = document.getElementById("patient-visits-list");
const cards = getAllCards();
cards.then((allCards)=>{
    allCards.forEach(card => render(card, patientVisitsList))
});


