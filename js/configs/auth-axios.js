import {siteConfig} from "./site-config.js";
// const token = localStorage.getItem("token");
const token = 'b57976980b03';               // имитируем сохранение токена после авторизации
const authReq = axios.create({
    baseURL: siteConfig.baseURL,
    headers: {
        Authorization: `Bearer ${token}`
    }
});

export {authReq};